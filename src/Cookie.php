<?php

namespace Velcoda\Helpers;

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Config;

class Cookie
{
    const FRONTEND_COOKIE_NAME = 'velcoda_spa';

    public static function getFrontEndCookie($cookie_name = self::FRONTEND_COOKIE_NAME, $content = null) {
        if (!$content) {
            $content = Auth::id();
        }
        return cookie(
            $cookie_name,
            $content,
            Config::get('session.lifetime'),
            '/',
            env('SESSION_DOMAIN'),
            !(env('APP_ENV') === 'local'),
            false
        );
    }
}
