<?php

namespace Velcoda\Helpers;

use Velcoda\Exceptions\Exceptions\HTTP_UNPROCESSABLE_ENTITY;

class PhoneNumber
{
    /**
     * @param      $number
     * @param bool $allow_anonymous
     *
     * @return string|null
     * @throws \Exception
     */
    public static function cleanPhoneNumber($number, $allow_anonymous = true):?string {
        if ($allow_anonymous && $number === 'anonymous') {
            return $number;
        }
        if ($number === 'null') {
            return null;
        }
        $phoneUtil = \libphonenumber\PhoneNumberUtil::getInstance();

        try {
            $num = $phoneUtil->parse($number, 'AT');
        } catch (\libphonenumber\NumberParseException $e) {
            inspector()->reportException($e);
            return $number; // if the number cannot be parsed, ignore it, and return original value
        }

        return $phoneUtil->format($num, \libphonenumber\PhoneNumberFormat::E164);
    }
}
