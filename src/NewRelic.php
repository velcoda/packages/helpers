<?php

namespace Velcoda\Helpers;

class NewRelic
{
    public static function setName() {
        $env = env('APP_ENV');
        $project = env('APP_PROJECT', 'velcoda');
        $name = env('APP_NAME');
        if (extension_loaded('newrelic')) { // Ensure PHP agent is available
            newrelic_set_appname(strtoupper($env) . ' – ' . strtoupper($project) . ' – '. $name);
        }
    }
}
