<?php

namespace Velcoda\Helpers;

class Changeset
{
    private $set = [];

    public function __construct()
    {
    }

    public function getSet()
    {
        return $this->set;
    }

    public function addToSet($key, $old_value, $new_value)
    {
        if (str_contains($key, 'password')) {
            $this->set[$key] = [
                'old_value' => '*****',
                'new_value' => '*****',
            ];
        } else {
            $this->set[$key] = [
                'old_value' => $old_value,
                'new_value' => $new_value,
            ];;
        }
    }
}
